<h1>Tools to turn documents into files you can analyze</h1>

Presentation link https://tinyurl.com/NICAR22BrownBag

You ask for the records to be provided as digital records. You push back time and time again. But the government agency isn't cooperating. 

Turning a PDF into a spreadsheet or a text file you can analyze is an important skill. Today, I'm going to show you five different ways to perform this task, and explain some of the benefits and drawbacks of each approach. 

<h3>Easy tools for pretty documents</h3>
<h4>[CometDocs](https://www.cometdocs.com/)</h4>

Sometimes, officials have your best interests in mind. They want your job to be easy. They provide records in a timely fashion. But they don't go the extra mile to get you the delimited file your heart desires.

That's ok. There are plenty of tools on the web that can help you transform your PDF into a spreadsheet for analysis. The first one I'll show you is [Cometdocs](https://www.cometdocs.com/).

CometDocs is a GUI that uses a combination of OCR and other tools to convert PDFs into other file types. It's relatively consistent, and works well on a variety of files. It works less well with the "ran it 12 times through the copy machine to prevent you from doing math" documents that a few unnamed agencies provide you. 

**How it works**

Website is https://www.cometdocs.com/

- If you have an account, login at the top right.
- Press the green Upload button on the top left. Add the Pritzker PDF.
- Click the Convert tab. Drag and drop the PDF into the white box.
- Select "to Excel (xlsx)" and click the Convert button.
- You will see a small PDF and XLSX box. The running conversion is visible on the white line under the Format section.
- Click the xlsx box, and agree to Download the file.
- On the next screen, click the "Click here to download your file" button to download the file.

A couple caveats to keep in mind - OCR isn't perfect. Sometimes, the letter S, the number 5 and a dollar sign look deceptively similar when it is being read with virtual eyes. Same goes for the number 0 and the letter O, and the letter l and the number 1. 

Also, CometDocs has a relatively good data sharing policy (i.e. they destroy these docs after storing them for a month). Other online services like I Love PDF, have similar functions to CometDocs, but they are less transparent than CometDocs about their document retention policies. Keep the sensitivity of your documents in mind when using a service like this.

Bonus good news for IRE members: your membership pays for a professional account with unlimited conversions. It also stores your docs ina  single place only you can access.

<h4>Adobe Acrobat</h4>

This is a tool that many (though not all) BGA employees have. It's great for combining doccument, rearranging pages and eliminating pages. But it can also turn a PDF into a text doc, a Word doc or a spreadsheet. 

**How it works**

- Open the Jo Daviess County document. 
- On the right toolbar (or from the tool search), select Export PDF.
- Select Spreadsheet - Microsof Excel workbook.
- Click the Export button, and select where you want to save the document.

<h3>Easy tools for slightly uglier documents</h3>
<h4>[I Love PDF](https://www.ilovepdf.com/) and [Tiny Wow](https://tinywow.com/)</h4>

I showed you a couple of (relatively) straightoforward docs. They're easy to read. They have the correct orientation. They are all tables. But everything isn't always this easy.

What do you do If you get a record where the first page is a FOIA response, and the other pages are a spreadsheet tuned sideways? If you have Adobe Acrobat, you can pretty easily cut pages and change orientation of files. If you don't, there are also online tools that will help. 

**How it works**

Website is https://www.ilovepdf.com/

- Click the Organize PDF link.
- Click the Select PDF file button. Open the Broadview file.
- Use the Delete Document x to eliminate the first two pages.
- Rotate the remaining pages three times each with the circle/arrow icon. 
- Click the Organize pages button.
- Click the Download file button.

This app also works to convert PDF files with built-in text to XLSX files, but not copied docs like this. You can run this organized document through Comet Docs or Adobe Acrobat to convert to a spreadsheet.

**How it works**

Website is https://tinywow.com/

- Click the Upload PDF link inside the PDF editor box.
- Select the Broadview PDF.
- Delete the first two pages with the trash can icon.
- Rotate each page three times with the circle/arrow icon.
- Click the blue Download button at the top right.


This app has additional functionality for editing video, images and audio files in addition to PDF editors. All of the functions are grouped by file type at the top (PDFs, images, file conversions, etc.). I saw this for the first time this year at NICAR. The wide range of functions is really impressive. Of the programs in this presentation, this one definitely gets pretty glitchy when a bunch of people are using it. But it's a really interesting collection of functions that could come in handy if it's a time crunch.

<h3>But what if I want to drive a muscle car?</h3>
<h4>[Tabula](https://tabula.technology/)</h4>

Tabula is a fantastic app that allows you to convert PDFs into spreadsheets locally rather than on the web. It's open-source and free. It also has great functions designed to work on freeing all of the useful data in PDFs. 

Since you have to download and configure this to work on your machine, I'll demo it here and you can come back and try it out if it seems useful to you. 

Notice the IP address - it operates in your web broswer, but Tabula operates locally. It also keeps track of your files, so the files you converted using Tabula in the past will remain on your page for some time. 

**How it works**

Website to download the program is https://tabula.technology/

- Click the button labeled Browse... under the header Import one or more PDFs.
- Select the Springfield PDF. Select the Import button. Bigger documents will load on the toolbar before the next screen loads. 
- There is more than one way to extract data, depending on how clean the documents are. It doesn't work with this document, but select Autodetect tables, which uses AI to pick up tabular data. Look at how it caught some of the tables but not others. Hit the Clear All Selections button.
- On the first page, use your mouse to trace a box over the table on the first page. At the bottom right of the box you just drew, click the Repeat this Selection button.
- Scroll through the doc to make sure it picked up all the tables. If it doesn't look ok, click the Clear All Selections button at the top of the page. If it does, click the Preview & Export Extracted Data button. Scroll down the page to make sure it picked things up, and click the Export button to download the file as a CSV. 

Tabula has a number of advantages that the web apps don't. Because it is hosted on your machine, you don't have to worry about what somebody else does with it. It can work on documents that CometDocs and some of the other apps fail at. The learning curve is a little steeper, but the community of users is large, so it's easy to find support if you get hung up on something.

<h3>Mobile solutions</h3>
<h4>[Office Suite for ios](https://apps.apple.com/us/app/microsoft-office/id541164041) and [Google/Droid](https://play.google.com/store/apps/details?id=com.microsoft.office.officehubrow)</h4>

Sometimes you are out in the world, and you aren't carrying a laptop with you. In that situation, how do you turn a document into something you can analyze?

I'll demo this one quickly. This one works (with the help of some pretty hefty AI) on photos you take with your phone. Printed documents, images on a screen at a meeting ... they can all be turned into data if it's presented clearly in tabular format.

**How it works**

- In the Microsoft Office mobile app, click the Actions tab.
- Select Image to Table.
- The app opens your camera. Snap a picture of your table. Make sure the photo is clear and that the image selects the table. Click the Confirm button.
- It will extract the data. The app asks questions about text or output it is unsure about. Click through the questions and make sure the cells are accurate.
- Open the file. It can be shared from here with a free account via email or text message as well as cloud drive applications.

<h3>Other interesting sessions from NICAR</h3>

The idea of IRE/NICAR is to give reporters skills and tools that help with investigatuve reporting. It's also imperitive that when somebody goes to these conferences that they brink back information that helps others in the newsroom.

Here is a collection of sessions I went to (and some I didn't) that could be helpful for you down the line. I will lead off with [the Lightning Talks](https://www.ire.org/2022-lightning-talks-lineup-announced/), which are always a NICAR favorite. As always, let me know if you have any questions.

Lightning Talks - [Video](https://vimeo.com/685047419)

**Politics**

Elections 2022: How to track online political messaging and ads - [Tipsheet](https://docs.google.com/document/d/16akkkKMEu0aVx5EHoL2y5tp3DT4LABxai_a7fTInuzs/edit) | [Slidedeck](https://docs.google.com/presentation/d/1UDDEtAxD4TGt7egS-Xi0aAI5F4mlbj6zBWUOLvGAq-k/edit#slide=id.p)

Foreign Influence resources - [Tipsheet](https://docs.google.com/document/d/13GFZyFX121OrXilujU5pHgLoXLFeXM059ikpVP-gVHY/edit) | [Slidedeck](https://docs.google.com/presentation/d/1VorrVgiX_jVdoEkktbVDx3GjF7Oa-gg2su3FFNTRKL8/edit#slide=id.p)


 **Criminal Justice**

 Law Enforcement Data: Telling Rich Stories for Print, Radio and National TV - [Tipsheet](https://docs.google.com/presentation/d/1BqTbFzA8wJAAOghaFUZmer2BVD-11NeEkZWY2X7Fupc/mobilepresent?slide=id.p) | [Tipsheet 2](https://docs.google.com/document/d/1P0bKTUQ-2MZQiSuGD-CbFXLf-EbHTfpsDqnBjrCwnHQ/edit?usp=sharing)

 Transforming the accessibility and transparency of federal courts - [Tipsheet](https://docs.google.com/document/d/1gjMeUw4W8UhjOIMzrhohrJezowhucC4rDntGaiLCEqk/edit#heading=h.yf9pnh1z9qrf)

**Education**

Covering the student debt crisis - [Tipsheet](https://mediacdn.guidebook.com/upload/156820/xfPpkyug0LsesEq5js4ZGqA8Kc6XGHJIOeUk.pdf)
 
 **Covering Diverse Communities**

 How to sensitively uncover issues & amplify voices in the Arab community - [Tipsheet](https://mediacdn.guidebook.com/upload/156820/pIFBr1HOe398LHnwuAIYUIWKhih3esRRRiJX.pdf) | [Video](https://vimeo.com/685012246)

 Tracking the Diversity of Your Sources - [Video](https://vimeo.com/685254590) | [Copy-and-Paste Tool](https://drive.google.com/drive/folders/1Nd4KbAXOySMy3o-I3_cJqJdDaF_PldTu)

 How does inequity and inequality show up on your beat? - [Slidedeck](https://docs.google.com/presentation/d/1myDF5kLGk65-0uYhM8v-gPxhaA8JzmRWeBgYJGW60E0/mobilepresent?slide=id.p)

 Best Practices to get and use data about the LGBTQ+ Community - [Video](https://vimeo.com/685185183)

**Other Reporting Topics**

Covering a disaster - [Tipsheet](https://vimeo.com/684954963/4e71156525) | [Video](https://vimeo.com/684955113/4f97997d60)

**Data Reporting**

How and why to make your data analysis reproducible - [Tipsheet](https://docs.google.com/document/d/12wSYUtAWEHYC3sTz6hsEeaYPsTuvr8lCOdZGi-fwHnA/edit#heading=h.xdnxi4rhmucz) | [Video](https://vimeo.com/685153771)

Data Dive with the Winners of the Philip Meyer Awards - [Video](https://vimeo.com/685230024)

Data Deep Dive: Disinformation - [Video](https://vimeo.com/685221014)

Artisinal Data Collection - [Video](https://vimeo.com/685285263)

Learning to Love Open Refine, a data cleaning app - [Lab](https://github.com/sneilsonsgit/NICAR22_OpenRefine)

Best Practices to get and use data - [Video](https://vimeo.com/685185183)
 
 **Public records**

 How to get data through state records laws - [Tipsheet](https://mediacdn.guidebook.com/upload/156820/PSG9bzFMImzpB1gFVFfSbrmd8hoentLH3jIY.pdf)

 Navigating the U.S. Freedom of Information Act - [Video](https://vimeo.com/685212228)

**For Editors**

Make Your Story Ironclad - [Slidedeck](https://mediacdn.guidebook.com/upload/156820/enc8O2H6D13poa43hZ7wJFAH4Kf71TuCnmMy.pdf)

Data for Editors - [Slidedecks](https://sites.google.com/view/mj-basic-data-academy/other-resources?authuser=0)

**Tech/Programming**

Mobile and desktop tools - [Video](https://vimeo.com/684954963/4e71156525)

Case studies in machine learning - [Tipsheet](https://docs.google.com/document/d/1P0bKTUQ-2MZQiSuGD-CbFXLf-EbHTfpsDqnBjrCwnHQ/edit?usp=sharing) | [Video](https://vimeo.com/684989191)

Python: Virtual Hands-on Lab - [Lab](https://docs.google.com/document/d/1CGuMvEjCAcsIEjFbNyCWgX3i4e1PQW_gs1XMFtn6exo/edit)

SQL: Virtual Hands-on Lab - [Lab](https://docs.google.com/document/d/1m9IbWQhdLW6wKpAMUeO3Bg8TsIn7_dh7tYqoEcJyC2I/edit)

R: Virtual Hands-on Lab - [Lab](https://docs.google.com/document/d/1-HjGUnpXlznnbWvsaOak9fhKztlCxePmMZ-du3vR0ck/edit)

Spreadsheets: Virtual Hands-on Lab - [Lab](https://docs.google.com/document/d/1fhmBI5AhMY0pNV1f9TT7sp0rk4XYlMynD_Jn929cYLk/edit)

Web Scraping: Virtual Hands-on Lab - [Lab](https://docs.google.com/document/d/1-7U3ulnXrQnwOI-67TEJM2yNMZ_nJ3-IMZo184bBAOc/edit)

Data Viz: Virtual Hands-on Lab - [Lab](https://docs.google.com/document/d/1-bjVTqB25P6sL1YX-wRJHsVPFH8E_72Bm9iwT0D9fOY/edit)
